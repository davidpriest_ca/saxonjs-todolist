<xsl:transform
	expand-text="yes"
	extension-element-prefixes="ixsl"
	version="3.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
	xmlns:js="http://saxonica.com/ns/globalJS"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:rh="http://red-house/rh/1.0"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- a persistent storage object -->
	<xsl:variable
		name="rh:todoStorage"
		select="ixsl:eval('window.localStorage')"></xsl:variable>
	<!-- TODO: move these to a package -->
	<xsl:function
		name="rh:clearStorage">
		<xsl:try
			select="ixsl:call($rh:todoStorage,'clear',[])">
			<xsl:catch>
				<xsl:message>Error code: <xsl:value-of
						select="$err:code"></xsl:value-of> Reason: <xsl:value-of
						select="$err:description"></xsl:value-of>
				</xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>
	<xsl:function
		name="rh:setTodo">
		<xsl:param
			as="xs:double"
			name="key"></xsl:param>
		<xsl:param
			as="xs:boolean"
			name="completed"></xsl:param>
		<xsl:param
			as="xs:string"
			name="label"></xsl:param>
		<xsl:try>
			<xsl:variable
				name="newItem">{{"completed":"{xs:string($completed)}","label":"{$label}"}}</xsl:variable>
			<xsl:sequence
				select="ixsl:call($rh:todoStorage,'setItem',[xs:string($key),xs:string($newItem)])"></xsl:sequence>
			<xsl:catch>
				<xsl:message>Error code: <xsl:value-of
						select="$err:code"></xsl:value-of> Reason: <xsl:value-of
						select="$err:description"></xsl:value-of>
				</xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>
	<xsl:function
		name="rh:removeTodo">
		<xsl:param
			as="xs:double"
			name="key"></xsl:param>
		<xsl:try
			select="ixsl:call($rh:todoStorage,'removeItem',[$key])">
			<xsl:catch>
				<xsl:message>Error code: <xsl:value-of
						select="$err:code"></xsl:value-of> Reason: <xsl:value-of
						select="$err:description"></xsl:value-of>
				</xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>

	<!-- called by index.html -->
	<xsl:template
		name="xsl:initial-template">
		<xsl:message>Entering transformation</xsl:message>
		<xsl:apply-templates
			mode="init"
			select="ixsl:page()"></xsl:apply-templates>
		<xsl:message>Exiting transformation</xsl:message>
	</xsl:template>

	<xsl:template
		match="section[@class='main']"
		mode="init updateList">
		<xsl:variable
			name="displayStyle"
			select="if ($rh:todoStorage?length = 0) then 'none' else 'block'"></xsl:variable>
		<xsl:sequence
			select="rh:footer($displayStyle)"></xsl:sequence>
		<xsl:apply-templates
			mode="#current"></xsl:apply-templates>
		<ixsl:set-style
			name="display"
			select="$displayStyle"></ixsl:set-style>
	</xsl:template>

	<xsl:function
		name="rh:footer">
		<xsl:param
			as="xs:string"
			name="display"></xsl:param>
		<ixsl:set-style
			name="display"
			object="ixsl:page()//footer[@class='footer']"
			select="$display"></ixsl:set-style>
	</xsl:function>

	<xsl:template
		match="input[@class='new-todo']"
		mode="#all">
		<xsl:sequence
			select="ixsl:call(self::*,'focus',[])"></xsl:sequence>
	</xsl:template>

	<xsl:template
		match="input[@class='new-todo']"
		mode="ixsl:onkeydown">
		<!-- keep an eye out for "enter" in the new todo box -->
		<xsl:if
			test="ixsl:event()?key eq 'Enter'">
			<!-- capture the new item -->
			<xsl:variable
				name="newLabel"
				select="xs:string(ixsl:get(self::*,'value'))"></xsl:variable>
			<xsl:if
				test="$newLabel!=''">
				<!-- add to storage -->
				<xsl:variable
					name="newKey"
					select="(max(ixsl:page()//ul[@id='todo-list']//input[@class='toggle']/@name) + 1,0)[1]"></xsl:variable>
				<xsl:sequence
					select="rh:setTodo($newKey,false(),$newLabel)"></xsl:sequence>
				<!-- update the world -->
				<xsl:apply-templates
					mode="updateList"
					select="ixsl:page()"></xsl:apply-templates>
				<!-- clear the new-todo entry -->
				<ixsl:set-property
					name="value"
					object="."
					select="''"></ixsl:set-property>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template
		match="ul[@id='todo-list']"
		mode="init updateList">
		<xsl:variable
			name="activeView"
			select="ixsl:page()//ul[@class='filters']//a[@class='selected']/@href"></xsl:variable>
		<xsl:result-document
			href="#todo-list"
			method="ixsl:replace-content">
			<xsl:for-each
				select="(0 to xs:integer($rh:todoStorage?length - 1))">
				<xsl:variable
					name="key"
					select="ixsl:call($rh:todoStorage,'key',[current()])"></xsl:variable>
				<xsl:variable
					name="jsonItem"
					select="ixsl:call($rh:todoStorage,'getItem',[$key])"></xsl:variable>
				<xsl:variable
					as="map(*)"
					name="item"
					select="parse-json($jsonItem)"></xsl:variable>
				<xsl:choose>
					<xsl:when
						test="($item?completed = 'true') and ($activeView='#/' or $activeView='#/completed')">
						<li
							class="completed">
							<div
								class="view">
								<input
									checked="true"
									class="toggle"
									name="{$key}"
									type="checkbox"></input>
								<label>{$item?label}</label>
								<button
									class="destroy"></button>
							</div>
							<input
								class="edit"
								value="{$item?label}"></input>
						</li>
					</xsl:when>
					<xsl:when
						test="($item?completed = 'false') and ($activeView='#/' or $activeView='#/active')">
						<li>
							<div
								class="view">
								<input
									class="toggle"
									name="{$key}"
									type="checkbox"></input>
								<label>{$item?label}</label>
								<button
									class="destroy"></button>
							</div>
							<input
								class="edit"
								value="{$item?label}"></input>
						</li>
					</xsl:when>
					<xsl:otherwise>
						<li
							class="{if ($item?completed = 'true') then 'completed ' else ''}hidden">
							<div>
								<input
									class="toggle"
									name="{$key}"
									type="checkbox">
									<!--<xsl:if
										test="$item?completed = 'true'">
										<xsl:attribute
											name="checked"
											select="'true'"></xsl:attribute>
									</xsl:if>-->
								</input>
								<label>{$item?label}</label>
								<button
									class="destroy"></button>
							</div>
							<input
								class="edit"
								value="{$item?label}"></input>
						</li>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:result-document>
	</xsl:template>

	<xsl:template
		match="span[@class='todo-count']"
		mode="init updateList">
		<xsl:result-document
			href="?."
			method="ixsl:replace-content">
			<xsl:variable
				name="count"
				select="count(ixsl:page()//ul[@id='todo-list']/li[not(contains(@class ,'completed'))])"></xsl:variable>
			<strong>{$count}</strong>
			<xsl:choose>
				<xsl:when
					test="$count = 1"> item left</xsl:when>
				<xsl:otherwise> items left</xsl:otherwise>
			</xsl:choose>
		</xsl:result-document>
	</xsl:template>

	<xsl:template
		match="ul[@class='filters']/li/a"
		mode="ixsl:click">
		<xsl:for-each
			select="ancestor::ul/li/a">
			<ixsl:set-attribute
				name="class"
				select="''"></ixsl:set-attribute>
		</xsl:for-each>
		<ixsl:set-attribute
			name="class"
			select="'selected'"></ixsl:set-attribute>
		<ixsl:set-property
			name="location.hash"
			select="@href"></ixsl:set-property>
		<xsl:apply-templates
			mode="updateList"
			select="ixsl:page()"></xsl:apply-templates>
	</xsl:template>

	<xsl:template
		match="input[@class='toggle']"
		mode="ixsl:click">
		<!-- flip the completed flag -->
		<xsl:sequence
			select="rh:setTodo(@name,not(exists(@checked)),following-sibling::label/text())"></xsl:sequence>
		<!-- update the lists & whatnot -->
		<xsl:apply-templates
			mode="updateList"
			select="ixsl:page()"></xsl:apply-templates>
	</xsl:template>

	<xsl:template
		match="input[@id='toggle-all']"
		mode="ixsl:click">
		<xsl:variable
			name="allCompleted"
			select="
			every $item in (following-sibling::ul[@id='todo-list']//input[@class='toggle']) 
			satisfies $item/@checked"></xsl:variable>
		<xsl:for-each
			select="following-sibling::ul[@id='todo-list']//input[@class='toggle']">
			<xsl:sequence
				select="rh:setTodo(@name,not($allCompleted),following-sibling::label/text())"></xsl:sequence>
		</xsl:for-each>
		<xsl:apply-templates
			mode="updateList"
			select="ixsl:page()"></xsl:apply-templates>
	</xsl:template>

	<xsl:template
		match="button[@class='destroy']"
		mode="ixsl:click">
		<xsl:sequence
			select="rh:removeTodo(preceding-sibling::input[@class='toggle']/@name)"></xsl:sequence>
		<xsl:apply-templates
			mode="updateList"
			select="ixsl:page()"></xsl:apply-templates>
	</xsl:template>

	<xsl:template
		match="button[@class='clear-completed']"
		mode="ixsl:click">
		<!-- remove completed tasks from the database -->
		<xsl:for-each
			select="ixsl:page()//input[@class='toggle' and @checked]">
			<xsl:sequence
				select="rh:removeTodo(@name)"></xsl:sequence>
		</xsl:for-each>
		<xsl:apply-templates
			mode="updateList"
			select="ixsl:page()"></xsl:apply-templates>
	</xsl:template>
	
	<!--TODO: Add double-click to edit -->
</xsl:transform>
